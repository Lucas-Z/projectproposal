﻿using UnityEngine;

[RequireComponent(typeof(BoxCollider2D))]

public class TrapObject : MonoBehaviour
{
    void Reset()
    {
        Debug.Log($"{name} Triggered");
        GetComponent<BoxCollider2D>().isTrigger = true;
    }

    private void OnTrigerEnter2D(Collider2D collision)
    {
        if(collision.tag == "Player")
        {
            Debug.Log($"{name} Triggered");
            FindObjectOfType<LevelManager>().Restart();
        }
    }
}
